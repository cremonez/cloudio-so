package controller;

import model.*;
import view.TelaPrincipal;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.*;

public class Usuario {
	RedeTCP conexaoGerente;
	DataCloud bdNuvem;
	Transacao transacaoArmazenarArquivo = new Transacao();
	Transacao transacaoListarPasta = new Transacao();
	Transacao transacaoDuplicarArquivos = new Transacao();
	Transacao transacaoCriarPasta = new Transacao();
	Transacao transacaoTratamentoQuedaHost = new Transacao();
	Pausa pausaDuplicarArquivos = new Pausa();
	public Usuario(TelaPrincipal tela) throws Exception{
		System.out.println("PC1");
		setTela(tela);
		bdNuvem = new DataCloud();
		bdNuvem.createFilesTable();
	}
	
	public void beginTransacoes(){
		transacaoArmazenarArquivo.begin();
		transacaoListarPasta.begin();
		transacaoDuplicarArquivos.begin();
		transacaoCriarPasta.begin();
		transacaoTratamentoQuedaHost.begin();
	}
	public void commitTransacoes(){
		transacaoArmazenarArquivo.commit();
		transacaoListarPasta.commit();
		transacaoDuplicarArquivos.commit();
		transacaoCriarPasta.commit();
		transacaoTratamentoQuedaHost.commit();
	}
	
	TelaPrincipal tela;
	public TelaPrincipal getTela() {
		return tela;
	}
	public void setTela(TelaPrincipal tela) {
		this.tela = tela;
	}
	
	// Entrar na conexao
	public void entrarConexao(String IP, int porta){
		conexaoGerente = new RedeTCP();
		try {
			conexaoGerente.entrarConexao(IP, porta);
			conexaoGerente.enviarTexto("inserir_tabela_arquivos_total");
			conexaoGerente.enviarTexto("????");
			conexaoGerente.enviarTexto("");
			conexaoGerente.enviarTexto(String.valueOf(0));
			conexaoGerente.receberDados(); conexaoGerente.receberTexto();//confirmacao
			iniciarConexoes();
		} catch (IOException e) {
			System.out.println("Gerente nao encontrado");
		}
	}
	
	// faz o tratamento de excecao para quando um host cai
	public void tratarQuedaUsuario(String IP, int porta){
		System.out.println("Comecou tratar queda de UsuariosA");
		beginTransacoes();
		System.out.println("Comecou tratar queda de UsuariosB");
		conexaoGerente.enviarTexto("deletar_dados_usuario");
		conexaoGerente.enviarTexto(IP);
		conexaoGerente.enviarTexto(String.valueOf(porta));
		conexaoGerente.receberDados(); conexaoGerente.receberTexto(); //confirmacao
		System.out.println("Terminou tratar queda de Usuarios");
		commitTransacoes();
		duplicarArquivos();
	}

	// Pede conselho para gerente de onde deve armazenar o arquivo e insere onde ele falou (true=sucesso,false=jaExiste)
	public boolean armazenarArquivo(String atualPath, String url){
		beginTransacoes();
		conexaoGerente.enviarTexto("armazenar_arquivo");
		conexaoGerente.enviarTexto(atualPath);
		conexaoGerente.enviarTexto(Tool.separeFileName(url));
		conexaoGerente.receberDados();
		int quantidade = Integer.parseInt((conexaoGerente.receberTexto()));
		if(quantidade >= 1){
			return false;
		}
		conexaoGerente.receberDados();
		String IP = conexaoGerente.receberTexto();
		conexaoGerente.receberDados();
		int porta = Integer.parseInt(conexaoGerente.receberTexto());
		// Printa apenas
		System.out.println("=Envie o arquivo " + url + " para=");
		System.out.println("IP: " + IP);
		System.out.println("Porta: " + porta);
		commitTransacoes();
		// PC1 -> PC2
		try {
			RedeTCP client = new RedeTCP();
			client.entrarConexao(IP, porta);
			client.receberDados(); client.receberTexto(); //confirmacao
			client.enviarTexto("save_file");
			client.enviarTexto(atualPath);
			client.enviarTexto(Tool.separeFileName(url));
			int tamanho = -1;
			try{
				tamanho = (int)(new FileInputStream(new File(url))).getChannel().size();
			}catch(Exception e){
				e.printStackTrace();
			}
			client.enviarTexto(String.valueOf(tamanho));
			client.enviarArquivo(url);
			client.receberDados(); client.receberTexto(); //confirmacao
		} catch (IOException e1) {
			System.out.println("**Queda usuario XXXX");
			tratarQuedaUsuario(IP, porta);
			System.out.println("**Queda usuario YYYY");
			armazenarArquivo(atualPath, url);
			System.out.println("**Queda usuario ZZZZ");
		}
		return true;
	}
	// Duplica os arquivos que estao em quantidade igual a 1 fazendo um backup para outro computador
	public void duplicarArquivos(){
		tela.setLoading(true);
		conexaoGerente.enviarTexto("duplicar_arquivos");
		System.out.println("---> Aguardar1");
		pausaDuplicarArquivos.aguardar();
		System.out.println("---> Aguardar2");
		conexaoGerente.receberDados(); conexaoGerente.receberTexto(); //confirmacao
		tela.setLoading(false);
	}
	//
	public void baixarArquivo(String pathOrigem, String pathSalvar){
		beginTransacoes();
		conexaoGerente.enviarTexto("baixar_arquivo");
		conexaoGerente.enviarTexto(pathOrigem);
		conexaoGerente.receberDados();
		String IP = conexaoGerente.receberTexto();
		conexaoGerente.receberDados();
		int porta = Integer.parseInt(conexaoGerente.receberTexto());
		commitTransacoes();
		// PC1 -> PC2
		System.out.println("SECONECTAR PC1 -> PC2");//
		RedeTCP client = new RedeTCP();
		try {
			client.entrarConexao(IP, porta);
			client.receberDados(); client.receberTexto(); //confirmacao
			client.enviarTexto("baixar_arquivo");
			client.enviarTexto(pathOrigem);
			client.receberDados();
			client.receberArquivo(pathSalvar);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	// Pede para gerente listar a pasta e retorna
	public ListaArquivos listarPasta(String url){
		beginTransacoes();
		System.out.println("**************************LISTAR PASTA1");
		ListaArquivos listaArquivos = new ListaArquivos();
		conexaoGerente.enviarTexto("listar_pasta");
		System.out.println("**************************LISTAR PASTA2a");
		conexaoGerente.enviarTexto(url);
		System.out.println("TIPO DE DADO->: "+conexaoGerente.receberDados());
		int quantidade = Integer.parseInt(conexaoGerente.receberTexto());
		System.out.println("**************************LISTAR PASTA2c");
		String[] listagem = new String[quantidade];
		int[] tipo = new int[quantidade];
		System.out.println("**************************LISTAR PASTA3");
		for(int i=0; i<quantidade; i++){
			conexaoGerente.receberDados();
			listagem[i] = conexaoGerente.receberTexto();
			conexaoGerente.receberDados();
			tipo[i] = Integer.parseInt(conexaoGerente.receberTexto());
		}
		listaArquivos.setListagem(listagem);
		listaArquivos.setTipo(tipo);
		commitTransacoes();
		return listaArquivos;
	}
	// Pede conselho para gerente de onde deve armazenar a pasta criada e insere onde
	public boolean criarPasta(String path, String nomePasta){
		beginTransacoes();
		conexaoGerente.enviarTexto("criar_pasta");
		conexaoGerente.enviarTexto(path);
		conexaoGerente.enviarTexto(nomePasta);
		conexaoGerente.receberDados();
		int quantidade = Integer.parseInt(conexaoGerente.receberTexto());
		commitTransacoes();
		if(quantidade >= 1){
			return false;
		}
		return true;
	}
	// Envia para o gerente atualizar a ultima porta aberta do usuario
	public void atualizarUltimaPorta(int ultimaPorta){
		beginTransacoes();
		conexaoGerente.enviarTexto("atualizar_ultima_porta");
		conexaoGerente.enviarTexto(String.valueOf(ultimaPorta));
		conexaoGerente.receberDados();conexaoGerente.receberTexto();//confirmacao
		getTela().setPorta(ultimaPorta);
		commitTransacoes();
	}
	
	
	
	
	// RECEBEDOR DE COMANDOS EXTERNOS
	// Cria uma nova conexao e fica aguardando comandos dos hosts
	public void iniciarConexoes() {
		new Thread(new Runnable() {
			public void run() {
				RedeTCP conexaoCliente = new RedeTCP();
				int porta = conexaoCliente.abrirConexao();
				atualizarUltimaPorta(porta);
				conexaoCliente.aguardarConexaoCliente();
				conexaoCliente.enviarTexto("conectado"+porta); //confirmacao
				iniciarConexoes();
				while (true) {
					if (conexaoCliente.receberDados() == RedeTCP.TEXTO) {
						String comando = conexaoCliente.receberTexto();
						if (comando.equals("create_folder")) {
							// Cria pasta...
						} else if (comando.equals("remove_folder")) {
							// Deleta pasta
						} else if (comando.equals("move_file")) {
							// Move arquivo
						} else if (comando.equals("remove_file")) {
							// Deleta arquivo
						} else if (comando.equals("enviar_arquivo_duplicar")) {//////////////////////////////////////////
							conexaoCliente.receberDados();
							String path = conexaoCliente.receberTexto();
							conexaoCliente.receberDados();
							String nome = conexaoCliente.receberTexto();
							conexaoCliente.receberDados();
							String IPToSend = conexaoCliente.receberTexto();
							conexaoCliente.receberDados();
							int portaToSend = Integer.parseInt(conexaoCliente.receberTexto());
							
							System.out.println(">>>>>>>>>>>>>>>>>>>>>>"+path);
							System.out.println(">>>>>>>>>>>>>>>>>>>>>>"+nome);
							System.out.println("=Duplicar Envie o arquivo " + "files/"+path+nome + " para=");
							System.out.println("IP: " + IPToSend);
							System.out.println("Porta: " + portaToSend);
							
							// PC1 -> PC2
							RedeTCP client = new RedeTCP();
							try {
								client.entrarConexao(IPToSend, portaToSend);
								client.receberDados(); client.receberTexto(); //confirmacao
								client.enviarTexto("save_file");
								client.enviarTexto(path);
								client.enviarTexto(nome);
								int tamanho = -1;
								try{
									tamanho = (int)(new FileInputStream(new File("files/"+path+nome))).getChannel().size();
								}catch(Exception e){
									e.printStackTrace();
								}
								client.enviarTexto(String.valueOf(tamanho));
								client.enviarArquivo("files/"+path+nome);
								// Confirmacao
								client.receberDados(); client.receberTexto();
								conexaoCliente.enviarTexto("ok");
							} catch (IOException e1) {
								System.out.println("ERRO 1919");
								tratarQuedaUsuario(IPToSend, portaToSend);
								duplicarArquivos();
							}
						} else if (comando.equals("save_file")) {
							System.out.println("ZZZZZZZZZZZZZZZZZZZZZZ");
							conexaoCliente.receberDados();
							String atualPath = conexaoCliente.receberTexto();
							conexaoCliente.receberDados();
							String nomeArquivo = conexaoCliente.receberTexto();
							conexaoCliente.receberDados();
							int tamanho = Integer.parseInt(conexaoCliente.receberTexto());
							File dir = new File("files/" + atualPath);
							if(!dir.exists()){
							    dir.mkdirs();
							}
							conexaoCliente.receberDados();
							conexaoCliente.receberArquivo("files/" + atualPath + nomeArquivo);
							System.out.println("********************save_file**** " + atualPath + " - "+nomeArquivo);
							// Insere na tabela de arquivos local
							String ip = null;
							try {
								ip = InetAddress.getLocalHost().getHostAddress();
							} catch (UnknownHostException e) {
								e.printStackTrace();
							}
							bdNuvem.insertFilesTable(nomeArquivo, atualPath, tamanho, ip, -1, 0);
							// Envia msg para gerente para atualizar o BD do
							beginTransacoes();
							conexaoGerente.enviarTexto("inserir_tabela_arquivos_total");
							conexaoGerente.enviarTexto(atualPath);
							conexaoGerente.enviarTexto(nomeArquivo);
							conexaoGerente.enviarTexto(String.valueOf(tamanho));
							// Confirmacao
							conexaoGerente.receberDados(); conexaoGerente.receberTexto();
							conexaoCliente.enviarTexto("realizado");
							commitTransacoes();
						} else if (comando.equals("baixar_arquivo")) {
							conexaoCliente.receberDados();
							String pathOrigem = conexaoCliente.receberTexto();
							System.out.println("<<<ENVIAR>>>"+"files/" + pathOrigem);//
							conexaoCliente.enviarArquivo("files/" + pathOrigem);
						} else if (comando.equals("termino_de_duplicacao")) {
							pausaDuplicarArquivos.terminar();
						}
						
						
					}
				}
			}
		}).start();
	}

}
