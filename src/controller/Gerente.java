package controller;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.*;
import java.util.List;

import model.*;
import view.TelaPontoAcesso;
import view.TelaPrincipal;

public class Gerente {
	public DataCloud bdNuvem;
	private static Transacao transacaoDuplicarArquivos = new Transacao();
	public Gerente(TelaPontoAcesso tela) throws Exception{
		System.out.println("Gerente");
		setTela(tela);
		bdNuvem = new DataCloud();
		//bdNuvem.connect();
		bdNuvem.createFilesTable();
		bdNuvem.insertHostsTable("127.0.0.1",-1,"gerente");
		//bdNuvem.updateHostTableUltimaPorta(5001, "127.0.0.1");
		iniciarConexoes();
	}
	TelaPontoAcesso tela;
	public TelaPontoAcesso getTela() {
		return tela;
	}
	public void setTela(TelaPontoAcesso tela) {
		this.tela = tela;
	}

	// Cria uma nova conexao e fica aguardando comandos dos hosts
	public void iniciarConexoes(){
		new Thread(new Runnable(){
            public void run(){
                RedeTCP conexaoCliente = new RedeTCP();
                int portaAberta = conexaoCliente.abrirConexao();
                getTela().setPorta(portaAberta);
            	conexaoCliente.aguardarConexaoCliente();
            	//bdNuvem.insertHostsTable("192.168.1.1", "gerente");
            	bdNuvem.insertHostsTable(conexaoCliente.getIP(),conexaoCliente.getPorta(),"usuario");
            	iniciarConexoes();
            	while(true){
            		if(conexaoCliente.receberDados() == RedeTCP.TEXTO){
            			String comando = conexaoCliente.receberTexto();
            			if(comando.equals("armazenar_arquivo")){
            				// Gerente pesquisa melhor pc para armazenar o arquivo
            				conexaoCliente.receberDados();
            				String path = conexaoCliente.receberTexto();
            				conexaoCliente.receberDados();
            				String nomeDado = conexaoCliente.receberTexto();
            				int quantidade = bdNuvem.selectQuantidadeDesteDado(path,nomeDado);
            				conexaoCliente.enviarTexto(String.valueOf(quantidade));
            				if((quantidade >= 1) == false){
            					Host menorHost = bdNuvem.selectMenorHost();
                				String host = menorHost.getHost();
                				int porta = menorHost.getUltimaPorta();
                				conexaoCliente.enviarTexto(host);
                				conexaoCliente.enviarTexto(String.valueOf(porta));	
            				}
            			}else if(comando.equals("baixar_arquivo")){
            				conexaoCliente.receberDados();
            				String pathOrigem = conexaoCliente.receberTexto();
            				System.out.println(">>>>"+pathOrigem);//
            				Host hostOrigem = bdNuvem.selectHostOrigemArquivo(Tool.subirPasta(pathOrigem),Tool.separeFileName(pathOrigem));
            				String host = hostOrigem.getHost();
            				int porta = hostOrigem.getUltimaPorta();
            				conexaoCliente.enviarTexto(host);
            				conexaoCliente.enviarTexto(String.valueOf(porta));	
            			}else if(comando.equals("listar_pasta")){
            				conexaoCliente.receberDados();
            				String url = conexaoCliente.receberTexto();
            				// lista o diretorio url
            				ListaArquivos listaArquivos = bdNuvem.selectListagemDiretorio(url);
            				String[] listagem = listaArquivos.getListagem();
            				int[] tipo = listaArquivos.getTipo();
            				System.out.println("*************** listou o diretoriooooooow");
            				int quantidade = listagem.length;// veirifica a quantidade de itens a serem listados
            				conexaoCliente.enviarTexto(String.valueOf(quantidade));
            				System.out.println("*************** listou o diretoriooooooow3");
            				for(int i=0; i<quantidade; i++){
            					conexaoCliente.enviarTexto(listagem[i]);
            					conexaoCliente.enviarTexto(String.valueOf(tipo[i]));
            				}
            				System.out.println("*************** listou o diretoriooooooow4");
            			}else if(comando.equals("criar_pasta")){
            				conexaoCliente.receberDados();
	            			String path = conexaoCliente.receberTexto();
	            			conexaoCliente.receberDados();
	            			String nomePasta = conexaoCliente.receberTexto();
	            			int quantidade = bdNuvem.selectQuantidadeDesteDado(path, nomePasta);
	            			conexaoCliente.enviarTexto(String.valueOf(quantidade));
	            			if(quantidade < 1){
	            				bdNuvem.insertFilesTable(nomePasta, path, 0, conexaoCliente.getIP(), conexaoCliente.getPorta(), 1);
	            			}
            			}else if(comando.equals("inserir_tabela_arquivos_total")){
            				// Insere na tabela de arquivos total
            				conexaoCliente.receberDados();
	            			String path = conexaoCliente.receberTexto();
            				conexaoCliente.receberDados();
	            			String nomeArquivo = conexaoCliente.receberTexto();
	            			conexaoCliente.receberDados();
	            			int tamanho = Integer.parseInt(conexaoCliente.receberTexto());
		            		bdNuvem.insertFilesTable(nomeArquivo, path, tamanho, conexaoCliente.getIP(), conexaoCliente.getPorta(), 0);
		            		//Confirmacao
		            		conexaoCliente.enviarTexto("realizado");
            			}else if(comando.equals("atualizar_ultima_porta")){
            				System.out.println("GERENTE ATUALIZA ULTIMA PORTA");
            				conexaoCliente.receberDados();
            				int ultimaPorta = Integer.parseInt(conexaoCliente.receberTexto());
            				System.out.println(conexaoCliente.getIP() +", "+ conexaoCliente.getPorta() +", "+ ultimaPorta);
            				bdNuvem.updateHostTableUltimaPorta(conexaoCliente.getIP(), conexaoCliente.getPorta(), ultimaPorta);
            				conexaoCliente.enviarTexto("ok");
            				pausa.terminar();
            				System.out.println("GERENTE TERMINA ATUALIZAR ULTIMA PORTA");
            			}else if(comando.equals("duplicar_arquivos")){
            				transacaoDuplicarArquivos.begin();
            				new Thread(new Runnable() {
            					List<Arquivo> arquivos = bdNuvem.selectArquivosNaoDuplicados();
            					public void run() {
            						//for(int i=0; i<arquivos.size(); i++){
            						if(bdNuvem.quantidadeHosts() > 1){
            							while((arquivos = bdNuvem.selectArquivosNaoDuplicados()) != null){System.out.println("TADUPLICANO******");
                							try {
                								int i = 0;
                								RedeTCP conexaoClienteOrigem = new RedeTCP();
												conexaoClienteOrigem.entrarConexao(arquivos.get(i).getHost(),arquivos.get(i).getPorta());
												conexaoClienteOrigem.receberDados(); conexaoClienteOrigem.receberTexto(); //confirmacao de conexao
	    			            				System.out.println("%%%%%%ANTES");
												pausa.aguardar();
												System.out.println("%%%%%%DEPOIS");
	    			            				conexaoClienteOrigem.enviarTexto("enviar_arquivo_duplicar");
	    			            				conexaoClienteOrigem.enviarTexto(arquivos.get(i).getPath());
	    			            				conexaoClienteOrigem.enviarTexto(arquivos.get(i).getNome());
	    			            				Host host = bdNuvem.selectHostDestinoDuplicata(arquivos.get(i).getPath(), arquivos.get(i).getNome());
	    			            				conexaoClienteOrigem.enviarTexto(host.getHost());
	    			            				conexaoClienteOrigem.enviarTexto(String.valueOf(host.getUltimaPorta()));
	    			            				System.out.println(arquivos.get(i).getPorta() + " -------> " + host.getUltimaPorta());
	    			            				conexaoClienteOrigem.receberDados(); conexaoClienteOrigem.receberTexto(); // confirmacao
											} catch (IOException e) {
												System.out.println("ERRO 998");
												break;
											}
            							}
            						}
            						transacaoDuplicarArquivos.commit();
            						RedeTCP conexaoConfirmacao = new RedeTCP();
            						int ultimaPorta = bdNuvem.selectUltimaPorta(conexaoCliente.getIP(), conexaoCliente.getPorta());
            						try {
										conexaoConfirmacao.entrarConexao(conexaoCliente.getIP(),ultimaPorta);
										//conexaoConfirmacao.receberDados(); String ok = conexaoConfirmacao.receberTexto();//confirmacao de conexao
										//System.out.println("confirmaco====>"+ok);
										conexaoConfirmacao.enviarTexto("termino_de_duplicacao");
										//pausa.aguardar();
										conexaoCliente.enviarTexto("ok");
									} catch (IOException e) {
										System.out.println("ERRO 1777 - Cliente que solicitou a duplicacao de arquivos na rede nao esta mais ativo!");
									}
            					}
            				}).start();
            			}else if(comando.equals("deletar_dados_usuario")){
            				conexaoCliente.receberDados();
            				String IP = conexaoCliente.receberTexto();
            				conexaoCliente.receberDados();
            				int porta = Integer.parseInt(conexaoCliente.receberTexto());
            				bdNuvem.deleteDadosHost(IP, porta);
            				conexaoCliente.enviarTexto("ok");
            			}
            			
            			
            			
            		}
            	}
        	}
        }).start();
	}
	Pausa pausa = new Pausa();
}
