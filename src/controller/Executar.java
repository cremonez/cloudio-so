package controller;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import model.ListaArquivos;
import view.*;

public class Executar {
	public static void main(String []args){
		new Welcome();
		Menu menu = new Menu();
		if(menu.getOption() == 0){
			// Tela para entrar em uma nuvem
			EnterCloud entrarNuvem = new EnterCloud();
			System.out.println("IP: "+entrarNuvem.getIP());
			System.out.println("Porta: "+entrarNuvem.getPorta());
			// Abre tela para recuperar comandos
			String[] listagem = {"Carregando"};
			int[] tipo = {TelaPrincipal.FILE};
			TelaPrincipal tela = new TelaPrincipal(listagem, tipo);
			// Se conectar ao gerente
			Usuario usuario = null;
			try{
				usuario = new Usuario(tela);
			}catch(Exception e){
				e.printStackTrace();
			}
			usuario.entrarConexao(entrarNuvem.getIP(),entrarNuvem.getPorta());
			ListaArquivos listaArquivo = usuario.listarPasta(tela.getAtualPath());
			tela.setLoading(false);
			tela.refreshList(listaArquivo);
			
			int comando;
			while((comando = tela.nextComando()) != TelaPrincipal.SAIR){
				if(comando == TelaPrincipal.ENVIAR_ARQUIVO){
					System.out.println("enviarArquivo(" + tela.getPath() + ")");
					tela.setLoading(true);
					if(usuario.armazenarArquivo(tela.getAtualPath(),tela.getPath()) == true){
						System.out.println("sucesso");
						usuario.duplicarArquivos();//
					}else{
						System.out.println("este arquivo ja existe");
						JOptionPane.showMessageDialog(null, "Este arquivo já existe nesta pasta!", "CloudIO",JOptionPane.WARNING_MESSAGE);
					}
					tela.setLoading(false);
					ListaArquivos listaArquivos = usuario.listarPasta(tela.getAtualPath());
					tela.refreshList(listaArquivos);
					
				}else if(comando == TelaPrincipal.DUPLICAR_ARQUIVOS){
					System.out.println("duplicarArquivos()");
					usuario.duplicarArquivos();
					System.out.println("Terminou de duplicar");
					
				}else if(comando == TelaPrincipal.BAIXAR_ARQUIVO){
					System.out.println("baixarArquivo("+tela.getPathOrigem()+"," + tela.getPath() + ")");
					usuario.baixarArquivo(tela.getPathOrigem(),tela.getPath());
				}else if(comando == TelaPrincipal.LISTAR_DIRETORIO){
					System.out.println("listarDiretorio(" + tela.getAtualPath() + ")");
					ListaArquivos listaArquivos = usuario.listarPasta(tela.getAtualPath());
					tela.refreshList(listaArquivos);
				}else if(comando == TelaPrincipal.CRIAR_PASTA){
					System.out.println("criarPasta(" + tela.getAtualPath()+ "," +tela.getNomePasta() + ")");
					if(usuario.criarPasta(tela.getAtualPath(),tela.getNomePasta()) == true){
						System.out.println("Pasta criada com sucesso!");
					}else{
						System.out.println("Esta pasta ja existe");
						JOptionPane.showMessageDialog(null, "Já existe uma pasta com esse nome!", "CloudIO",JOptionPane.WARNING_MESSAGE);
					}
					ListaArquivos listaArquivos = usuario.listarPasta(tela.getAtualPath());
					tela.refreshList(listaArquivos);
				}//else if....else if....
			}
		}else if(menu.getOption() == 1){
			System.out.println("Criar uma nuvem");
			String[] listagem = {};
			int[] tipo = {};
			TelaPontoAcesso tela = new TelaPontoAcesso(listagem, tipo);
			Gerente gerente = null;
			try{
				gerente = new Gerente(tela);
			}catch(Exception e){
				e.printStackTrace();
			}

			int comando;
			while((comando = tela.nextComando()) != TelaPrincipal.SAIR){
				if(comando == TelaPrincipal.ENVIAR_ARQUIVO){
					System.out.println("enviarArquivo(" + tela.getPath() + ")");	
				}//else if....else if....
			}
			
		}else{
			System.exit(1);
		}
		
		
		
	}
		
}
