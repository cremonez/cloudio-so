package controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import model.Arquivo;
import model.DataCloud;
import model.Host;

public class Estatisticas {
	public static void main(String []args) throws Exception{
		// Se conecta com BD
    	String sDriverName = "org.sqlite.JDBC";
		Class.forName(sDriverName);
		String bdURL = "jdbc:sqlite:"+"data.db";
		Connection conexaoBD = DriverManager.getConnection(bdURL);
		Statement statement = conexaoBD.createStatement();
		Statement statement2 = conexaoBD.createStatement();
		statement.setQueryTimeout(30);
		
		// Consulta os dados da tabela no BD
		System.out.println("=== ARQUIVOS ===");
		ResultSet result = statement.executeQuery("SELECT * FROM arquivo");
		while(result.next()){
			System.out.print("id: "+result.getString("id"));
			System.out.print("	- ");
			System.out.print("nome: "+result.getString("nome"));
			System.out.print("	- ");
			System.out.print("path: "+result.getString("path"));
			System.out.print("	- ");
			System.out.print("tamanho: "+result.getString("tamanho"));
			System.out.print("		- ");
			System.out.print("host: "+result.getString("host"));
			System.out.print("		- ");
			System.out.print("porta: "+result.getString("porta"));
			System.out.print("\n");
		}
		
		/*
		result = statement.executeQuery("SELECT SUM(tamanho) AS soma, host, porta FROM arquivo WHERE porta != -1 GROUP BY porta ORDER BY soma ASC LIMIT 1");
		String ip = result.getString("host");
		int porta = result.getInt("porta");
		System.out.println(ip+" - "+porta);
		result = statement.executeQuery("SELECT * FROM host WHERE host = '" + ip + "' AND porta = " + porta);
		
		//result = statement.executeQuery("SELECT SUM(tamanho) AS soma, host, porta FROM arquivo WHERE porta != -1 GROUP BY porta ORDER BY soma ASC LIMIT 1");
		//result = statement.executeQuery("SELECT tamanho AS soma, host FROM arquivo");

		while(result.next()){
			System.out.print("id: "+result.getString("id"));
			System.out.print("	- ");
			System.out.print("host: "+result.getString("host"));
			System.out.print("	- ");
			System.out.print("porta: "+result.getInt("porta"));
			System.out.print("	- ");
			System.out.print("ultimaPorta: "+result.getInt("ultimaPorta"));
			System.out.print("	- ");
			System.out.print("cargo: "+result.getString("cargo"));
			System.out.print("\n");
		}
		*/
		System.out.println("=== HOSTS ===");
		result = statement.executeQuery("SELECT * FROM host");
		while(result.next()){
			System.out.print("id: "+result.getString("id"));
			System.out.print("	- ");
			System.out.print("host: "+result.getString("host"));
			System.out.print("	- ");
			System.out.print("porta: "+result.getInt("porta"));
			System.out.print("	- ");
			System.out.print("ultimaPorta: "+result.getInt("ultimaPorta"));
			System.out.print("	- ");
			System.out.print("cargo: "+result.getString("cargo"));
			System.out.print("\n");
		}
		/*
		// Consulta os dados da tabela no BD
		System.out.println("=== ARQUIVOS ===");
		//result = statement.executeQuery("SELECT * FROM arquivo WHERE path = '"+ "" +"' AND nome = '"+ "grafo.dot" +"' AND porta != -1 LIMIT 1");
		//result = statement.executeQuery("SELECT * FROM arquivo WHERE path = '"+ "" +"' AND nome = '"+ "grafo.dot" +"' AND porta != -1 LIMIT 1");
		result = statement.executeQuery("SELECT SUM(tamanho) AS soma, host, porta FROM arquivo WHERE porta != -1 AND path != '"+ "" +"' AND nome != '"+ "lixeira.png" +"' GROUP BY host,porta ORDER BY soma ASC LIMIT 1");
		while(result.next()){
			//System.out.print("id: "+result.getString("id"));
			//System.out.print("	- ");
			//System.out.print("nome: "+result.getString("nome"));
			//System.out.print("	- ");
			//System.out.print("path: "+result.getString("path"));
			//System.out.print("	- ");
			//System.out.print("tamanho: "+result.getString("tamanho"));
			//System.out.print("		- ");
			System.out.print("host: "+result.getString("host"));
			System.out.print("		- ");
			
			System.out.print("porta: "+result.getInt("porta"));
			System.out.print("\n");
		}
        //int quantidade = statement.executeQuery("SELECT COUNT(*) AS quantidade FROM arquivo WHERE path = 'pastaXXX/' AND porta != -1").getInt("quantidade");
		
		
		
		System.out.println("==============================================");
		DataCloud bdNuvem = new DataCloud();
		bdNuvem.connect();
		List<Arquivo> arquivos = bdNuvem.selectArquivosNaoDuplicados();
		for(int i=0; i<arquivos.size();i++){
			System.out.println(">>Arquivo nao duplicado "+i+"<<");
			System.out.println("path: "+arquivos.get(i).getPath());
			System.out.println("nome: "+arquivos.get(i).getNome());
			System.out.println("host: "+arquivos.get(i).getHost());
			System.out.println("port: "+arquivos.get(i).getPorta());
			System.out.println(">Host destino<");
			Host host = bdNuvem.selectHostDestinoDuplicata(arquivos.get(i).getPath(), arquivos.get(i).getNome());
			System.out.println("host: "+host.getHost());
			System.out.println("port: "+host.getUltimaPorta());
			System.out.println("\n\n\n");
		}
		
		System.out.println(bdNuvem.quantidadeHosts());
		
		DataCloud bdNuvem = new DataCloud();
		//bdNuvem.deleteDadosHost("127.0.0.1",5005);
		// Consulta os dados da tabela no BD
		System.out.println("=== ARQUIVOS ===");
		result = statement.executeQuery("SELECT * FROM arquivo");
		while(result.next()){
			System.out.print("id: "+result.getString("id"));
			System.out.print("	- ");
			System.out.print("nome: "+result.getString("nome"));
			System.out.print("	- ");
			System.out.print("path: "+result.getString("path"));
			System.out.print("	- ");
			System.out.print("tamanho: "+result.getString("tamanho"));
			System.out.print("		- ");
			System.out.print("host: "+result.getString("host"));
			System.out.print("		- ");
			System.out.print("porta: "+result.getString("porta"));
			System.out.print("\n");
		}
		
		System.out.println("=== HOSTS ===");
		result = statement.executeQuery("SELECT * FROM host");
		while(result.next()){
			System.out.print("id: "+result.getString("id"));
			System.out.print("	- ");
			System.out.print("host: "+result.getString("host"));
			System.out.print("	- ");
			System.out.print("porta: "+result.getInt("porta"));
			System.out.print("	- ");
			System.out.print("ultimaPorta: "+result.getInt("ultimaPorta"));
			System.out.print("	- ");
			System.out.print("cargo: "+result.getString("cargo"));
			System.out.print("\n");
		}
		*/
		DataCloud bdNuvem = new DataCloud();
		System.out.println(bdNuvem.selectArquivosNaoDuplicados());
		conexaoBD.close();
	}
}
