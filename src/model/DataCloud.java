package model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DataCloud {
	Statement statement,statement2;
	Connection conexaoBD;
	public void connect() throws Exception{
		// Se conecta com BD
    	String driverSql = "org.sqlite.JDBC";
		Class.forName(driverSql);
		String bdURL = "jdbc:sqlite:"+"data.db";
		conexaoBD = DriverManager.getConnection(bdURL);
		statement = conexaoBD.createStatement();
		statement2 = conexaoBD.createStatement();
		statement.setQueryTimeout(30);
		statement2.setQueryTimeout(30);
	}
	public void disconnect(){
		try {
			statement.close();
			statement2.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	// Cria a tabela arquivo se ela ainda nao foi criada
	public void createFilesTable(){
		try{
			connect();
			statement.executeUpdate("CREATE TABLE arquivo (id INTEGER PRIMARY KEY AUTOINCREMENT, nome TEXT, path TEXT, tamanho INTEGER, host TEXT, porta INTEGER, tipo INTEGER)");
			statement.executeUpdate("CREATE TABLE host (id INTEGER PRIMARY KEY AUTOINCREMENT, host TEXT, porta INTEGER, ultimaPorta INTEGER, cargo TEXT)");
			disconnect();
		}catch(Exception e){}
	}
	// Insere uma tupla na tabela arquivo
	public void insertFilesTable(String nome, String path, int tamanho, String host, int porta, int tipo){
		try{
			connect();
			statement.executeUpdate("INSERT INTO arquivo (nome,path,tamanho,host,porta,tipo) VALUES ('"+nome+"','"+path+"',"+tamanho+",'"+host+"',"+porta+","+tipo+")");
			disconnect();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	// Insere uma tupla na tabela de hosts
	public void insertHostsTable(String host, int porta, String cargo){
		try{
			connect();
			statement.executeUpdate("INSERT INTO host (host,porta,cargo) VALUES ('"+host+"','"+porta+"','"+cargo+"')");
			disconnect();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	// Atualiza a ultima porta aberta
	public void updateHostTableUltimaPorta(String ip, int porta, int ultimaPorta){
		try{
			connect();
			statement.executeUpdate("UPDATE host SET ultimaPorta = " + ultimaPorta + " WHERE host='" + ip + "' AND porta=" + porta);
			disconnect();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	// retorna a ultima porta aberta do host:porta
	public int selectUltimaPorta(String ip, int porta){
		int ultimaPorta = -1;
		try{
			connect();
			ResultSet result = statement.executeQuery("SELECT ultimaPorta FROM host WHERE host='" + ip + "' AND porta=" + porta);
			ultimaPorta = result.getInt("ultimaPorta");
			disconnect();
		}catch(Exception e){
			e.printStackTrace();
		}
		return ultimaPorta;
	}
	//
	public Host selectMenorHost(){
		Host host = new Host();
		try {
			connect();
			ResultSet result;
			//result = statement.executeQuery("SELECT * FROM host WHERE id = 2");
			//result = statement.executeQuery("SELECT SUM(tamanho) AS soma, host, porta FROM arquivo WHERE porta != -1 GROUP BY host,porta ORDER BY soma ASC LIMIT 1");
			result = statement.executeQuery("SELECT SUM(tamanho) AS soma, host, porta FROM arquivo WHERE porta != -1 GROUP BY host,porta ORDER BY soma ASC LIMIT 1");
			String ip = result.getString("host");
			int porta = result.getInt("porta");
			result = statement.executeQuery("SELECT * FROM host WHERE host = '" + ip + "' AND porta = " + porta);
			
			if(result.next()){
				host.setId(result.getInt("id"));
				host.setHost(result.getString("host"));
				host.setPorta(result.getInt("porta"));
				host.setUltimaPorta(result.getInt("ultimaPorta"));
			}
			disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return host;
	}
	//
	public Host selectHostOrigemArquivo(String path, String nomeArquivo){
		Host host = new Host();
		try {
			connect();
			System.out.println(path+ " - "+nomeArquivo);
			ResultSet result;
			result = statement.executeQuery("SELECT * FROM arquivo WHERE path = '"+ path +"' AND nome = '"+ nomeArquivo +"' AND porta != -1 LIMIT 1");
			String ip = result.getString("host");
			int porta = result.getInt("porta");
			result = statement.executeQuery("SELECT * FROM host WHERE host = '" + ip + "' AND porta = " + porta);
			if(result.next()){
				host.setId(result.getInt("id"));
				host.setHost(result.getString("host"));
				host.setPorta(result.getInt("porta"));
				host.setUltimaPorta(result.getInt("ultimaPorta"));
			}
			disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return host;
	}
	// Recupera a listagem dos arquivos na pasta url
	public ListaArquivos selectListagemDiretorio(String url){
		ListaArquivos listaArquivos = null;
		try {
			connect();
			int quantidade = statement.executeQuery("SELECT COUNT(*) AS quantidade FROM arquivo WHERE path = '"+ url +"' AND porta != -1").getInt("quantidade");
			String[] listagem = new String[quantidade];
			int[] tipo = new int[quantidade];
			ResultSet result = statement.executeQuery("SELECT * FROM arquivo WHERE path = '"+ url +"' AND porta != -1");
			int i=0;
			while(result.next()){
				listagem[i] = result.getString("nome");
				tipo[i] = result.getInt("tipo");
				i++;
			}
			listaArquivos = new ListaArquivos();
			listaArquivos.setListagem(listagem);
			listaArquivos.setTipo(tipo);
			disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listaArquivos;
	}
	//
	public int selectQuantidadeDesteDado(String path, String nomeDado){
		int quantidade = -1;
		try {
			connect();
			quantidade = statement.executeQuery("SELECT COUNT(*) AS quantidade FROM arquivo WHERE path = '"+ path +"' AND nome='"+nomeDado+"' AND porta != -1").getInt("quantidade");
			disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return quantidade;
	}
	
	// Recupera dados da tabela arquivo
	private void selectFilesTable(){
		ResultSet result;
		try {
			connect();
			result = statement.executeQuery("SELECT * FROM arquivo");
			while(result.next()){
				System.out.print("id: "+result.getString("id"));
				System.out.print("	- ");
				System.out.print("nome: "+result.getString("nome"));
				System.out.print("	- ");
				System.out.print("path: "+result.getString("path"));
				System.out.print("	- ");
				System.out.print("tamanho: "+result.getString("tamanho"));
				System.out.print("		- ");
				System.out.print("host: "+result.getString("host"));
				System.out.print("\n");
			}
			disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	//
	public List<Arquivo> selectArquivosNaoDuplicados(){
		List<Arquivo> listaArquivos = new ArrayList();
		Arquivo arquivo;
		try {
			connect();
			ResultSet result = statement.executeQuery("SELECT COUNT(*) AS quantidade FROM host WHERE cargo = 'usuario'");
			int quantidadeDeHostsAtivos = result.getInt("quantidade");
			if(quantidadeDeHostsAtivos <= 1){
				return null;
			}
			result = statement.executeQuery("SELECT COUNT(*) AS quantidade,host,porta,nome,path,tamanho FROM arquivo WHERE tipo = 0 AND porta != -1 AND path != '????' GROUP BY path,nome HAVING quantidade = 1");
			while(result.next()){
				arquivo = new Arquivo();
				arquivo.setId(result.getInt("quantidade"));
				arquivo.setHost(result.getString("host"));
				arquivo.setNome(result.getString("nome"));
				arquivo.setPath(result.getString("path"));
				arquivo.setTamanho(result.getInt("tamanho"));
				ResultSet result2 = statement2.executeQuery("SELECT * FROM host WHERE host = '" + result.getString("host") + "' AND porta = " + result.getInt("porta"));
				arquivo.setPorta(result2.getInt("ultimaPorta"));
				listaArquivos.add(arquivo);
			}
			disconnect();
		} catch (Exception e){
			e.printStackTrace();
		}
		if(listaArquivos.size() == 0){
			return null;
		}
		return listaArquivos;
	}
	//
	public Host selectHostDestinoDuplicata(String path, String nomeArquivo){
		Host host = new Host();
		try {
			connect();
			ResultSet result, result2;
			result = statement.executeQuery("SELECT SUM(tamanho) AS soma, host, porta FROM arquivo WHERE porta != -1 GROUP BY host,porta ORDER BY soma ASC");
			while(result.next()){
				result2 = statement2.executeQuery("SELECT * FROM host WHERE host = '" + result.getString("host") + "' AND porta = " + result.getInt("porta"));
				host.setHost(result2.getString("host"));
				host.setPorta(result2.getInt("porta"));
				host.setUltimaPorta(result2.getInt("ultimaPorta"));
				if(hostContemArquivo(host,path,nomeArquivo) == false){
					break;
				}
			}
			disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return host;
	}
	public boolean hostContemArquivo(Host host, String path, String nomeArquivo){
		try {
			ResultSet result = statement2.executeQuery("SELECT COUNT(*) AS quantidade FROM arquivo WHERE host = '"+host.getHost()+"' AND porta = "+host.getPorta()+" AND path = '"+path+"' AND nome = '"+nomeArquivo+"'");
			//disconnect();
			if(result.getInt("quantidade") == 0){
				return false;
			}else{
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	//
	public int quantidadeHosts(){
		int quantidade = -1;
		try {
			connect();
			ResultSet result = statement.executeQuery("SELECT COUNT(*) AS quantidade FROM host WHERE cargo = 'usuario'");
			quantidade = result.getInt("quantidade");
			disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return quantidade;
	} 
	//
	public void deleteDadosHost(String IP, int porta){
		try {
			connect();
			ResultSet result = statement.executeQuery("SELECT * FROM host WHERE host = '"+IP+"' AND ultimaPorta = "+porta);
			if(result.next()){
				porta = result.getInt("porta");	
			}
			statement.executeUpdate("DELETE FROM arquivo WHERE host = '"+IP+"' AND porta = "+porta);
			statement.executeUpdate("DELETE FROM host WHERE host = '"+IP+"' AND porta = "+porta);
			disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
