///////////////////////////////////////////////////////////
//////////// CRATED BY: VITOR MASSARO CREMONEZ ////////////
////////////////////// BETA VERSION /////////////////////// 
///////////////////////////////////////////////////////////
package model;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class RedeTCP {
	// Variaveis
	private static final int BUFFER = 4096;
	public static final byte VAZIO = 0, TEXTO = 1, ARQUIVO = 2;
	private byte tipoDado;
	private Socket cliente;
	private ServerSocket servidor;
	private InputStream canalEntrada;
	private OutputStream canalSaida;
	private PrintStream  printSaida;
	private int dataSize;
	private String IP;
	private int Porta;
	
	public String getIP(){
		return IP;
	}
	private void setIP(String IP){
		this.IP = IP;
	}
	public int getPorta(){
		return Porta;
	}
	private void setPorta(int Porta){
		this.Porta = Porta;
	}
	
	//private String extension;
	public RedeTCP(){
		tipoDado = VAZIO;	
	}
	// Se coneta a uma rede ja existente
	public void entrarConexao(String IP, int porta) throws IOException{
		try{
			cliente = new Socket(IP, porta);
			canalEntrada = cliente.getInputStream();
			try{
				canalSaida = cliente.getOutputStream();
			}catch(IOException e){
				e.printStackTrace();
			}
			printSaida = new PrintStream(canalSaida);
			System.out.println("Conectado ao host");
		}catch(UnknownHostException e){
			e.printStackTrace();
		}
	}
	// Abre uma nova conexao e retorna a porta pela qual foi aberta
	public int abrirConexao(){
		int porta = 5000;
		for(int i=porta;i<65536;i++){
			try{
				porta = i;
				servidor = new ServerSocket(porta);
				System.out.println("Abriu a porta "+porta);
				break;
			}catch(IOException e){
				//e.printStackTrace();
			}
		}
		try{
			setIP(InetAddress.getLocalHost().getHostAddress());
			setPorta(porta);
		}catch(UnknownHostException e){
			e.printStackTrace();
		}
		return porta;
	}
	// Fica a espera de uma conexao com a porta ja aberta
	public void aguardarConexaoCliente(){
		try{
			cliente = servidor.accept();
			canalEntrada = cliente.getInputStream();
			System.out.println("Servidor reconheceu cliente");
			setIP(((InetSocketAddress)cliente.getRemoteSocketAddress()).getAddress().getHostAddress());//
			setPorta(cliente.getPort());//
		}catch(IOException e){
			e.printStackTrace();
		}
		try{
			canalSaida = cliente.getOutputStream();
		}catch(IOException e){
			e.printStackTrace();
		}
		printSaida = new PrintStream(canalSaida);
	}
	// Envia um array de bytes para o outro lado da rede conectada
	private void enviarDados(byte arrayBytes[]){
		for(int i=0; i<arrayBytes.length;i++){
			printSaida.write(arrayBytes[i]);
		}
	}
	// Faz o envio de dados do tipo texto
	public void enviarTexto(String string){
		int sizeString = string.length();
		byte arrayBytes[] = new byte[sizeString+5];
		arrayBytes[0] = TEXTO;
		arrayBytes[1] = intToBytes(sizeString, 3);
		arrayBytes[2] = intToBytes(sizeString, 2);
		arrayBytes[3] = intToBytes(sizeString, 1);
		arrayBytes[4] = intToBytes(sizeString, 0);
		for(int i=5; i<sizeString+5;i++){
			arrayBytes[i] = (byte)string.charAt(i-5);
		}
		enviarDados(arrayBytes);
	}
	// Faz o envio de dados do tipo arquivo na localiza��o do sistema "url"
	public void enviarArquivo(String url){
		try {
			FileInputStream file = new FileInputStream(new File(url));
			int sizeFile;
			try{
				sizeFile = (int)file.getChannel().size();
				byte arrayBytes[] = new byte[sizeFile+5];
				arrayBytes[0] = ARQUIVO;
				arrayBytes[1] = intToBytes(sizeFile, 3);
				arrayBytes[2] = intToBytes(sizeFile, 2);
				arrayBytes[3] = intToBytes(sizeFile, 1);
				arrayBytes[4] = intToBytes(sizeFile, 0);
				for(int i=5; i<sizeFile+5; i++){
					arrayBytes[i] = (byte) file.read();
				}
				enviarDados(arrayBytes);
			}catch(IOException e){
				e.printStackTrace();
			}
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}
	}
	// Retona o tipo de dado lido VAZIO/TEXTO/ARQUIVO
	public byte receberDados(){
		if(tipoDado == VAZIO){
			try{
				tipoDado = (byte)canalEntrada.read();
				if(tipoDado != VAZIO){
					dataSize = bytesToInt((byte)canalEntrada.read(), (byte)canalEntrada.read(), (byte)canalEntrada.read(), (byte)canalEntrada.read());
				}
			}catch(IOException e){
				e.printStackTrace();
			}
		}
		return tipoDado;
	}
	// Retorna em um array de bytes os dados chegados pela rede
	public byte[] receberBytes(){
		byte[] arrayBytes = new byte[dataSize];
		int byteLido;
		try{
			for(int i=0;i<dataSize;i++){
				byteLido = canalEntrada.read();
				arrayBytes[i] = (byte)byteLido;
				System.out.flush();
			}
		}catch(IOException e){
			e.printStackTrace();
		}
		tipoDado = VAZIO;
		return arrayBytes;
	}
	// DEVELOPMENT // DEVELOPMENT // DEVELOPMENT // DEVELOPMENT // DEVELOPMENT // DEVELOPMENT // DEVELOPMENT // DEVELOPMENT //
	public String receberTexto(){
		return bytesToText(receberBytes());
	}
	public void receberArquivo(String url){
		byte[] buffer = new byte[BUFFER];
		FileOutputStream arquivo = null;
		try{
			arquivo = new FileOutputStream(new File(url));
		}catch(FileNotFoundException e1){
			e1.printStackTrace();
		}
		int bytesLidos;
		try{
			for(int i=0;i<dataSize;i++){
				bytesLidos = canalEntrada.read(buffer,0,Math.min(BUFFER, dataSize-i));
				arquivo.write(buffer,0,bytesLidos);
				i += bytesLidos-1;
				System.out.flush();
			}
			arquivo.close();
			tipoDado = VAZIO;
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void enviarArquivo(String url, int x){
		byte[] buffer = new byte[BUFFER];
		try {
			FileInputStream arquivo = new FileInputStream(new File(url));
			int dataSize;
			try{
				dataSize = (int)arquivo.getChannel().size();
				int bytesLidos;
				printSaida.write(ARQUIVO);
				printSaida.write(intToBytes(dataSize, 3));
				printSaida.write(intToBytes(dataSize, 2));
				printSaida.write(intToBytes(dataSize, 1));
				printSaida.write(intToBytes(dataSize, 0));
				for(int i=0;i<dataSize;i++){
					bytesLidos = arquivo.read(buffer,0,Math.min(BUFFER, dataSize-i));
					printSaida.write(buffer,0,bytesLidos);
					i += bytesLidos-1;
					//System.out.println(i);
				}
			}catch(IOException e){
				e.printStackTrace();
			}
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}
	}
	// DEVELOPMENT // DEVELOPMENT // DEVELOPMENT // DEVELOPMENT // DEVELOPMENT // DEVELOPMENT // DEVELOPMENT // DEVELOPMENT //
	
	// Converte o array de bytes em uma string
	private String bytesToText(byte[] arrayBytes){
		char[] arrayChars = new char[arrayBytes.length];
		for(int i=0;i<arrayBytes.length;i++){
			arrayChars[i] = (char)arrayBytes[i];
		}
		String string = new String(arrayChars);
		return string;
	}
	// Converte o array de bytes em um arquivo salvando no caminho Url
	public void bytesToFile(byte arrayBytes[],String url){
		try {
			FileOutputStream arquivo = new FileOutputStream(new File(url));
			for(int i=0;i<arrayBytes.length;i++){
				try{
					arquivo.write(arrayBytes[i]);
				}catch(IOException e){
					e.printStackTrace();
				}
			}
			try{
				arquivo.close();
			}catch(IOException e){
				e.printStackTrace();
			}
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}
	}

	
	// VERIFICA SE O TIPO DE ARQUIVO (PUBLIC)

	// Converte 4 bytes em 1 inteiro
	private static int bytesToInt(byte b1, byte b2, byte b3, byte b4){
		int inteiro = (b1&255)*16777216 + (b2&255)*65536 + (b3&255)*256 + (b4&255); 
		return inteiro;
	}
	// Converte 1 inteiro em 4 bytes byte[indice de 0-3]
	private static byte intToBytes(int inteiro, int indice){
		byte[] arrayBytes = new byte[4];
		arrayBytes[3] = (byte)(inteiro/16777216);
		arrayBytes[2] = (byte)((inteiro - ((arrayBytes[3]&255)*16777216))/65536);   
		arrayBytes[1] = (byte)((inteiro - ((arrayBytes[2]&255)*65536))/256); 
		arrayBytes[0] = (byte)(inteiro - ((arrayBytes[1]&255)*256));
		return arrayBytes[indice];
	}
	
}
