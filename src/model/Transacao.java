package model;

public class Transacao {
	private final int ABERTO = 0, FECHADO = 1;
	private int semaforo = ABERTO;
	
	public void begin(){
		if(semaforo == FECHADO){
			aguardar();
		}
		semaforo = FECHADO;
	}
	
	public void commit(){
		semaforo = ABERTO;
		terminar();
	}
	
	// Aguarda a chamada do metodo terminar() para continuar a leitura normal do codigo
	private void aguardar(){
		synchronized(this){
			try{
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	// Executa o termino da pausa
	private void terminar(){
		synchronized(this){
			this.notify();
		}
	}
}
