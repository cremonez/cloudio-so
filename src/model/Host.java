package model;

public class Host {
	private int id;
	private String host;
	private int porta;
	private int ultimaPorta;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public int getPorta() {
		return porta;
	}
	public void setPorta(int porta) {
		this.porta = porta;
	}
	public int getUltimaPorta() {
		return ultimaPorta;
	}
	public void setUltimaPorta(int ultimaPorta) {
		this.ultimaPorta = ultimaPorta;
	}
	
	
}
