package model;
public class Pausa extends Thread{
	// Aguarda a chamada do metodo terminar() para continuar a leitura normal do codigo
	public void aguardar(){
		synchronized(this){
			try{
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	// Executa o termino da pausa
	public void terminar(){
		synchronized(this){
			this.notify();
		}
	}
}
