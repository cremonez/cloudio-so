package model;

import java.util.ArrayList;
import java.util.List;

public class ListaArquivos {
	String[] listagem;
	int[] tipo;
	
	public String[] getListagem() {
		return listagem;
	}
	public void setListagem(String[] listagem) {
		this.listagem = listagem;
	}
	public int[] getTipo() {
		return tipo;
	}
	public void setTipo(int[] tipo) {
		this.tipo = tipo;
	}
	
	public ListaArquivos retirarDuplicatas(){
		ListaArquivos novaListaArquivos = new ListaArquivos();
		List<String> listaArquivos = new ArrayList();
		List<Integer> listaTipos = new ArrayList();
		
		for(int i=0; i<listagem.length; i++){
			if(listaArquivos.contains(getListagem()[i]) == false){
				listaArquivos.add(getListagem()[i]);
				listaTipos.add(getTipo()[i]);
			}
		}
		int tamanho = listaArquivos.size();
		String[] listagem = new String[tamanho];
		int[] tipo = new int[tamanho];
		for(int i=0; i<listagem.length; i++){
			listagem[i] = listaArquivos.get(i);
			tipo[i] = listaTipos.get(i);
		}
		novaListaArquivos.setListagem(listagem);
		novaListaArquivos.setTipo(tipo);
		return novaListaArquivos;
	}
	
}




