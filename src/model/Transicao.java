package model;
import javax.swing.JPanel;

public class Transicao extends Thread{
	// VARIAVEIS
	public static final int LINEAR = 0, FAST_SLOW = 1, SLOW_FAST = 2, SLOW_FAST_SLOW = 3, FAST_VERYSLOW = 4, VERYSLOW_FAST = 5;
	private JPanel panel, _panel;
	private Thread thread, _thread;
	private int startX, startY, finalX, finalY, effect, fps = 50, interval, iterations;
	private int _startX, _startY, _finalX, _finalY, _wait, _duration, _effect;
	private boolean _haveStart;
	private double status;
	// MOVER
	public void mover(JPanel panel, int startX, int startY, int finalX, int finalY, int wait, int duration, int effect){
		if(wait > 0){
			this._haveStart = true;
			this._panel = panel;
			this._startX = startX;
			this._startY = startY;
			this._finalX = finalX;
			this._finalY = finalY;
			this._wait = wait;
			this._duration = duration;
			this._effect = effect;
			this._thread = new Thread(this);
			this._thread.start();
		}else{
			mover(panel, startX, startY, finalX, finalY, duration, effect);
		}
	}	
	public void mover(JPanel panel, int startX, int startY, int finalX, int finalY, int duration, int effect){
		stopThread();
		this.panel = panel;
		this.startX = startX;
		this.startY = startY;
		this.finalX = finalX;
		this.finalY = finalY;
		this.effect = effect;
		this._wait = 0;
		this.status = 0;
		this.interval = (int)(1000/(double)fps);
		this.iterations = (int)(duration*fps/1000);
		this.thread = new Thread(this);
		this.thread.start();
	}
	// DEVELOPMENT // DEVELOPMENT // DEVELOPMENT // DEVELOPMENT // DEVELOPMENT // DEVELOPMENT // DEVELOPMENT // DEVELOPMENT //
	public void mover(JPanel panel, int finalX, int finalY, int wait, int duration, int effect){
		if(wait > 0){
			this._haveStart = false;
			this._panel = panel;
			this._finalX = finalX;
			this._finalY = finalY;
			this._wait = wait;
			this._duration = duration;
			this._effect = effect;
			this._thread = new Thread(this);
			this._thread.start();
		}else{
			mover(panel, finalX, finalY, duration, effect);
		}
	}	
	public void mover(JPanel panel, int finalX, int finalY, int duration, int effect){
		stopThread();
		this.panel = panel;
		this.startX = panel.getX();
		this.startY = panel.getY();
		this.finalX = finalX;
		this.finalY = finalY;
		this.effect = effect;
		this._wait = 0;
		this.status = 0;
		this.interval = (int)(1000/(double)fps);
		this.iterations = (int)(duration*fps/1000);
		this.thread = new Thread(this);
		this.thread.start();
	}
	// DEVELOPMENT // DEVELOPMENT // DEVELOPMENT // DEVELOPMENT // DEVELOPMENT // DEVELOPMENT // DEVELOPMENT // DEVELOPMENT //
	@SuppressWarnings("deprecation")
	private void stopThread(){
		if(thread != null){
			thread.stop();
		}
	}
	// ATUALIZAR
	private void atualizar(){
		if(status < 1){
			aplicarMovimento();
		}else{
			panel.setBounds(finalX, finalY, panel.getWidth(), panel.getHeight());
			stopThread();
		}
		status += (1/(double)iterations);
	}
	private void aplicarMovimento(){
		if (effect == LINEAR){
			panel.setBounds((int)(startX + (double)(finalX - startX) * linear(status)), (int)(startY + (double)(finalY - startY) * linear(status)), panel.getWidth(), panel.getHeight());
        }else if(effect == FAST_SLOW){
        	panel.setBounds((int)(startX + (double)(finalX - startX) * fast_slow(status)), (int)(startY + (double)(finalY - startY) * fast_slow(status)), panel.getWidth(), panel.getHeight());
        }else if(effect == SLOW_FAST){
        	panel.setBounds((int)(startX + (double)(finalX - startX) * slow_fast(status)), (int)(startY + (double)(finalY - startY) * slow_fast(status)), panel.getWidth(), panel.getHeight());
        }else if(effect == SLOW_FAST_SLOW){
        	panel.setBounds((int)(startX + (double)(finalX - startX) * slow_fast_slow(status)), (int)(startY + (double)(finalY - startY) * slow_fast_slow(status)), panel.getWidth(), panel.getHeight());
        }else if(effect == FAST_VERYSLOW){
        	panel.setBounds((int)(startX + (double)(finalX - startX) * fast_veryslow(status)), (int)(startY + (double)(finalY - startY) * fast_veryslow(status)), panel.getWidth(), panel.getHeight());
        }else if(effect == VERYSLOW_FAST){
        	panel.setBounds((int)(startX + (double)(finalX - startX) * veryslow_fast(status)), (int)(startY + (double)(finalY - startY) * veryslow_fast(status)), panel.getWidth(), panel.getHeight());
        }
	}
	// EXECUTAR
	public void run(){
		if(_wait != 0){
			try{
				Thread.sleep(_wait);
			}catch(InterruptedException e){
				e.printStackTrace();
			}
			stopThread();
			if(_haveStart == true){
				mover(_panel, _startX, _startY, _finalX, _finalY, _duration, _effect);
			}else{
				mover(_panel, _finalX, _finalY, _duration, _effect);
			}
		}else{
			while(true){
				atualizar();
				try{
					Thread.sleep(interval);
				}catch(InterruptedException e){
					e.printStackTrace();
				}
			}
		}
	}
	// FUNCOES
	private double linear(double x){
		return x;
	}
	private double fast_slow(double x){
        return x + ((Math.sin(Math.PI * x)) / Math.PI);
    }
    private double slow_fast(double x){
        return x - ((Math.sin(Math.PI * x)) / Math.PI);
    }
    private double slow_fast_slow(double x){
        return x - ((Math.sin(2 * Math.PI * x)) / (2 * Math.PI));
    }
	private double fast_veryslow(double x){
        return 1 - Math.pow(x - 1, 4);
    }
	private double veryslow_fast(double x){
		return Math.pow(x, 4);
	}
	
}



