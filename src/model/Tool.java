package model;

import java.io.File;

public class Tool {
	// Retorna somente o nome do arquivo+extensao da url inteira passada como parametro
	public static String separeFileName(String url){
		File f = new File(url);
		return f.getName();
	}
	// Retorna o path um nivel acima
	public static String subirPasta(String url){
		File f = new File(url);
		String newURL = f.getParent();
		if(newURL == null){
			return "";
		}else{
			return newURL+"/";
		}
	}
	
}
