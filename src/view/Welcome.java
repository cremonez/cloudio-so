package view;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

import model.*;

public class Welcome extends JFrame{
	Pausa pausa = new Pausa();
	JLabel labelImagemJogo,labelEntrar;
	JPanel panelImagemJogo,panelEntrar;
	Transicao transicaoImagemJogo = new Transicao();
	Transicao transicaoStart = new Transicao();
	public Welcome(){
		this.setTitle("CloudIO");
		this.setBackground(Color.WHITE);
		
		// IMAGEM NUVEM DE FUNDO
		labelImagemJogo = new JLabel();
		labelImagemJogo.setBounds(0,0,600,600);
		labelImagemJogo.setIcon(new ImageIcon("media/welcome.png"));
		panelImagemJogo = new JPanel();
		panelImagemJogo.setLayout(null);
		panelImagemJogo.setBackground(Color.WHITE);
		panelImagemJogo.setBounds(0, -600, 600, 600);
		panelImagemJogo.add(labelImagemJogo);		
		
		// IMAGEM ENTRAR
		labelEntrar = new JLabel();
		labelEntrar.setBounds(0,0,400,150);
		labelEntrar.setIcon(new ImageIcon("media/Start.png"));
		labelEntrar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panelEntrar = new JPanel();
		panelEntrar.setLayout(null);
		panelEntrar.setBackground(Color.WHITE);
		panelEntrar.setBounds(100, 450+300, 400, 150);
		panelEntrar.add(labelEntrar);
		labelEntrar.addMouseListener(new MouseAdapter(){public void mouseClicked(MouseEvent e){sair();}});
		
		JLayeredPane lpane = new JLayeredPane();
		lpane.setBounds(0, 0, 600, 600);
		lpane.add(panelImagemJogo, new Integer(0), 0);
		lpane.add(panelEntrar, new Integer(1), 0);
		
		this.setLayout(null);
		this.setVisible(true);
		this.setSize(600, 600);
		this.add(lpane);
		this.setLocationRelativeTo(null);
		this.getContentPane().setBackground(new Color(255, 255, 255));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		chegar();
		pausa.aguardar();
	}

	// EXECUTA OS EFEITOS DE TRANSICAO DE CHEGADA DOS COMPONENTES NA JANELA
	private void chegar(){
		transicaoImagemJogo.mover(panelImagemJogo, 0, 0, 0, 1300, Transicao.FAST_VERYSLOW);
		transicaoStart.mover(panelEntrar, 100, 450, 0, 1300, Transicao.FAST_VERYSLOW);
	}
	// EXECUTA OS EFEITOS DE TRANSICAO DE SAIDA E FINALIZA A JANELA
	private void sair(){
		transicaoImagemJogo.mover(panelImagemJogo, 0, panelImagemJogo.getY() - 1000, 0, 800, Transicao.VERYSLOW_FAST);
		transicaoStart.mover(panelEntrar, 100, 450+300, 0, 2300, Transicao.FAST_VERYSLOW);
		new java.util.Timer().schedule(new java.util.TimerTask(){public void run(){fecharJanela();}},1700);
	}
	// FECHA A JANELA E VOLTA A EXECUTAR O CODIGO DO LOCAL ONDE FOI CHAMADA A FUNCAO
	private void fecharJanela(){
		this.dispose();
		pausa.terminar();
	}
}
