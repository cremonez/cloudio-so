package view;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import model.*;

public class EnterCloud extends JFrame{
	Transicao transicaoEnterConexion = new Transicao();
	Transicao transicaoMsg1 = new Transicao();
	Transicao transicaoIP = new Transicao();
	Transicao transicaoPorta = new Transicao();
	Transicao transicaoEntrar = new Transicao();
	Transicao transicaoMsg2 = new Transicao();
	Transicao transicaoMenu = new Transicao();
	Pausa pausa = new Pausa();
	JFrame janela;
	JLabel labelEnterConexion, labelMsg1, labelTextoIP, labelTextoPorta, labelEntrar, labelMsg2, labelMenu;
	JTextField textFieldIP, textFieldPorta;
	JPanel panelEnterConexion, panelMsg1, panelIP, panelPorta, panelEntrar, panelMsg2, panelMenu;
	private String IP;
	private int Porta;

	public EnterCloud(){
		
		// IMAGEM NUVEM
		labelMenu = new JLabel();
		labelMenu.setBounds(0,0,600,600);
		labelMenu.setIcon(new ImageIcon("./media/Menu.png"));
		panelMenu = new JPanel();
		panelMenu.setLayout(null);
		panelMenu.setBackground(Color.WHITE);
		panelMenu.setBounds(0, 1000, 600, 600);
		panelMenu.add(labelMenu);
		
		
		// TEXTO VOCE ABRIU UMA NOVA CONEXAO
		labelEnterConexion = new JLabel("Entre em uma núvem já existente!", JLabel.CENTER);
		labelEnterConexion.setFont(new Font("Verdana",Font.PLAIN,30));
		labelEnterConexion.setForeground(Color.WHITE);
		labelEnterConexion.setBackground(Color.decode("#1DB0ED"));
		labelEnterConexion.setSize(600, 60);
		labelEnterConexion.setOpaque(true);
		panelEnterConexion = new JPanel();
		panelEnterConexion.setLayout(null);
		panelEnterConexion.setBounds(0, 130+1000, 600, 60);
		panelEnterConexion.add(labelEnterConexion);

		// TEXTO MSG1
		labelMsg1 = new JLabel("Insira o IP e Porta do Ponto de Acesso da nuvem existente para entrar nela:", JLabel.CENTER);
		labelMsg1.setFont(new Font("Verdana",Font.PLAIN,12));
		labelMsg1.setForeground(Color.WHITE);
		labelMsg1.setBackground(Color.decode("#1DB0ED"));
		labelMsg1.setSize(600, 60);
		labelMsg1.setOpaque(true);
		panelMsg1 = new JPanel();
		panelMsg1.setLayout(null);
		panelMsg1.setBounds(0, 190+1000, 600, 60);
		panelMsg1.add(labelMsg1);
		
		// REQUERIMENTO IP
		labelTextoIP = new JLabel("IP: ", JLabel.CENTER);
		labelTextoIP.setFont(new Font("Verdana",Font.PLAIN,36));
		labelTextoIP.setForeground(Color.WHITE);
		labelTextoIP.setBackground(Color.decode("#1DB0ED"));
		labelTextoIP.setSize(600, 60);
		labelTextoIP.setOpaque(true);
		textFieldIP = new JTextField();
		textFieldIP.setPreferredSize(new Dimension(300,50));
		textFieldIP.setFont(new Font("Verdana",Font.PLAIN,36));
		panelIP = new JPanel();
		panelIP.setLayout(new FlowLayout());
		panelIP.setBackground(Color.decode("#1DB0ED"));
		panelIP.setBounds(0, 250+1000, 600, 60);
		panelIP.add(labelTextoIP);
		panelIP.add(textFieldIP);
	
		// REQUERIMENTO PORTA
		labelTextoPorta = new JLabel("Porta: ", JLabel.CENTER);
		labelTextoPorta.setFont(new Font("Verdana",Font.PLAIN,36));
		labelTextoPorta.setForeground(Color.WHITE);
		labelTextoPorta.setBackground(Color.decode("#1DB0ED"));
		labelTextoPorta.setSize(600, 60);
		labelTextoPorta.setOpaque(true);
		textFieldPorta = new JTextField();
		textFieldPorta.setPreferredSize(new Dimension(240,50));
		textFieldPorta.setFont(new Font("Verdana",Font.PLAIN,36));
		panelPorta = new JPanel();
		panelPorta.setLayout(new FlowLayout());
		panelPorta.setBackground(Color.decode("#1DB0ED"));
		panelPorta.setBounds(0, 310+1000, 600, 60);
		panelPorta.add(labelTextoPorta);
		panelPorta.add(textFieldPorta);	
		
		// IMAGEM ENTRAR
		labelEntrar = new JLabel();
		labelEntrar.setBounds(0,0,400,150);
		labelEntrar.setIcon(new ImageIcon("./media/Enter.png"));
		labelEntrar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panelEntrar = new JPanel();
		panelEntrar.setLayout(null);
		panelEntrar.setBackground(Color.decode("#1DB0ED"));
		panelEntrar.setBounds(100, 370+1000, 400, 150);
		panelEntrar.add(labelEntrar);
		labelEntrar.addMouseListener(new MouseAdapter(){public void mouseClicked(MouseEvent e){guardarDados();sair();}});
		
		// TEXTO MSG2
		labelMsg2 = new JLabel("Quando você entrar na nuvem podera ver, armazenar e baixar os arquivos da nuvem.", JLabel.CENTER);
		labelMsg2.setFont(new Font("Verdana",Font.PLAIN,12));
		labelMsg2.setForeground(Color.WHITE);
		labelMsg2.setBackground(Color.decode("#1DB0ED"));
		labelMsg2.setSize(600, 60);
		labelMsg2.setOpaque(true);
		panelMsg2 = new JPanel();
		panelMsg2.setLayout(null);
		panelMsg2.setBounds(0, 520+1000, 600, 60);
		panelMsg2.add(labelMsg2);
		
		
		JLayeredPane lpane = new JLayeredPane();
		lpane.setBounds(0, 0, 600, 600);
		lpane.add(panelEnterConexion, new Integer(6), 0);
		lpane.add(panelMsg1, new Integer(5), 0);
		lpane.add(panelIP, new Integer(4), 0);
		lpane.add(panelPorta, new Integer(3), 0);
		lpane.add(panelEntrar, new Integer(2), 0);
		lpane.add(panelMsg2, new Integer(1), 0);
		lpane.add(panelMenu, new Integer(0), 0);
		
      	
		// JANELA
		this.setLayout(null);
		this.pack();
		this.getContentPane().setBackground(Color.WHITE);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.add(lpane);
		this.setSize(600, 600);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		
		chegar();
		pausa.aguardar();
	}
	// EXECUTA OS EFEITOS DE TRANSICAO DE CHEGADA DOS COMPONENTES NA JANELA
	private void chegar(){
		transicaoMenu.mover(panelMenu, 0, 0, 0, 1000, Transicao.FAST_VERYSLOW);
		transicaoEnterConexion.mover(panelEnterConexion, 0, 130, 300, 1000, Transicao.FAST_VERYSLOW);
		transicaoMsg1.mover(panelMsg1, 0, 190, 600, 1000, Transicao.FAST_VERYSLOW);
		transicaoIP.mover(panelIP, 0, 250, 900, 1000, Transicao.FAST_VERYSLOW);
		transicaoPorta.mover(panelPorta, 0, 310, 1200, 1000, Transicao.FAST_VERYSLOW);
		transicaoEntrar.mover(panelEntrar, 100, 370, 1500, 1000, Transicao.FAST_VERYSLOW);
		transicaoMsg2.mover(panelMsg2, 0, 520, 1800, 1000, Transicao.FAST_VERYSLOW);
	}
	// EXECUTA OS EFEITOS DE TRANSICAO DE SAIDA E FINALIZA A JANELA
	private void sair(){
		transicaoMenu.mover(panelMenu, 0, 0 + 1000, 1800, 1000, Transicao.VERYSLOW_FAST);
		transicaoEnterConexion.mover(panelEnterConexion, 0, 130 + 1000, 1500, 1000, Transicao.VERYSLOW_FAST);
		transicaoMsg1.mover(panelMsg1, 0, 190 + 1000, 1200, 1000, Transicao.VERYSLOW_FAST);
		transicaoIP.mover(panelIP, 0, 250 + 1000, 900, 1000, Transicao.VERYSLOW_FAST);
		transicaoPorta.mover(panelPorta, 0, 310 + 1000, 600, 1000, Transicao.VERYSLOW_FAST);
		transicaoEntrar.mover(panelEntrar, 150, 370 + 1000, 300, 1000, Transicao.VERYSLOW_FAST);
		transicaoMsg2.mover(panelMsg2, 0, 520 + 1000, 0, 1000, Transicao.VERYSLOW_FAST);
		new java.util.Timer().schedule(new java.util.TimerTask(){public void run(){fecharJanela();}},3000);
	}
	// GUARDA O IP e PORTA DIGITADA
	private void guardarDados(){
		setIP(textFieldIP.getText());
		setPorta(Integer.parseInt(textFieldPorta.getText()));
	}
	// FECHA A JANELA E VOLTA A EXECUTAR O CODIGO DO LOCAL ONDE FOI CHAMADA A FUNCAO
	private void fecharJanela(){
		this.dispose();
		pausa.terminar();
	}
	public String getIP(){
		return IP;
	}
	private void setIP(String IP){
		this.IP = IP;
	}
	public int getPorta(){
		return Porta;
	}
	private void setPorta(int Porta){
		this.Porta = Porta;
	}
}
