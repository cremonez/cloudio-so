package view;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

import model.ListaArquivos;
import model.Pausa;
import model.Tool;
import model.Transicao;

public class TelaPontoAcesso extends JFrame {
	public static final int ENVIAR_ARQUIVO = 0, LISTAR_DIRETORIO = 1, CRIAR_PASTA = 2, BAIXAR_ARQUIVO = 3, DUPLICAR_ARQUIVOS = 4, SAIR = -1;
	public static final int FILE = 0, FOLDER = 1;
	public String path, nomePasta, pathOrigem;
	

	public int comando;
	public int ultimaPorta;
	private boolean loading;
	
	Transicao transicaoLoading = new Transicao();
	Transicao transicaoTopCloud = new Transicao();
	Transicao transicaoIp = new Transicao();
	Transicao transicaoPorta = new Transicao();
	Transicao transicaoGerente = new Transicao();
	Transicao transicaoScroll = new Transicao();
	Transicao transicaoIcon = new Transicao();
	Transicao transicaoNomeArquivo = new Transicao();
	Transicao transicaoPathArquivo = new Transicao();
	Transicao transicaoPonto = new Transicao();
	Transicao transicaoButton1 = new Transicao();
	Transicao transicaoButton2 = new Transicao();
	Transicao transicaoButton3 = new Transicao();
	Transicao transicaoButton4 = new Transicao();
	Transicao transicaoButton5 = new Transicao();
	String atualPath = "";
	Pausa pausa = new Pausa();
	JPanel panelIp, panelNomeArquivo, panelIcon, panelPathArquivo, panelTopCloud, panelScroll, panelButtonEnviarArquivo,
			panelButtonCriarPasta, panelButtonMoveArquivo, panelButtonDelete, panelButtonListarDiretorio, panelPorta, panelGerente, panelLoading,panelUp,panelPonto;
	JLabel labelNomeArquivo, labelIcon, labelPathArquivo, labelTopCloud,labelPorta;
	JLayeredPane lpane;
	JList list;
	String[] listagem;
	int[] tipo;
	
	public String getPathOrigem() {
		return pathOrigem;
	}
	public void setPathOrigem(String pathOrigem) {
		this.pathOrigem = pathOrigem;
	}
	public String getNomePasta() {
		return nomePasta;
	}
	public void setNomePasta(String nomePasta) {
		this.nomePasta = nomePasta;
	}
	public void setPorta(int ultimaPorta){
		this.ultimaPorta = ultimaPorta;
		labelPorta.setText(String.valueOf(ultimaPorta));
	}
	public String[] getListagem() {
		return listagem;
	}
	public void setListagem(String[] listagem) {
		this.listagem = listagem;
	}
	public int[] getTipo() {
		return tipo;
	}

	public void setTipo(int[] tipo) {
		this.tipo = tipo;
	}
	public boolean isLoading() {
		return loading;
	}
	public void setLoading(boolean loading) {
		this.loading = loading;
		if(loading == true){
			transicaoLoading.mover(panelLoading, 0, 0, 0, 500, Transicao.FAST_VERYSLOW);
		}else{			
			transicaoLoading.mover(panelLoading, 0, -1000, 0, 500, Transicao.FAST_VERYSLOW);
		}
	}

	public TelaPontoAcesso(String[] listagem, int[] tipo) {
		setListagem(listagem);
		setTipo(tipo);
		// IMAGEM TOPO NUVEM
		labelTopCloud = new JLabel();
		labelTopCloud.setBounds(0, 0, 600, 80);
		labelTopCloud.setIcon(new ImageIcon("./media/Top_cloud_white.png"));
		panelTopCloud = new JPanel();
		panelTopCloud.setLayout(null);
		panelTopCloud.setBackground(new Color(0,0,0,0));
		panelTopCloud.setBounds(0, -100, 600, 80);
		panelTopCloud.add(labelTopCloud);

		// IP
		JLabel labelIp = null;
		try {
			labelIp = new JLabel("IP: " + InetAddress.getLocalHost().getHostAddress(), JLabel.CENTER);
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}
		labelIp.setFont(new Font("Verdana", Font.PLAIN, 14));
		labelIp.setForeground(Color.decode("#1DB0ED"));
		labelIp.setBackground(Color.WHITE);
		labelIp.setSize(200, 20);
		labelIp.setOpaque(true);
		panelIp = new JPanel();
		panelIp.setLayout(null);
		panelIp.setBounds(0, -100, 200, 20);
		panelIp.add(labelIp);
		// PORTA
		labelPorta = null;
		labelPorta = new JLabel("Porta: " + ultimaPorta);
		labelPorta.setFont(new Font("Verdana", Font.PLAIN, 14));
		labelPorta.setForeground(Color.decode("#1DB0ED"));
		labelPorta.setBackground(Color.WHITE);
		labelPorta.setSize(200, 20);
		labelPorta.setOpaque(true);
		panelPorta = new JPanel();
		panelPorta.setLayout(null);
		panelPorta.setBounds(200, 0-100, 200, 20);
		panelPorta.add(labelPorta);
		
		// IMAGEM GERENTE
		JLabel labelGerente = new JLabel();
		labelGerente.setBounds(0, 0, 100, 25);
		labelGerente.setIcon(new ImageIcon("./media/Gerente.png"));
		panelGerente = new JPanel();
		panelGerente.setLayout(null);
		panelGerente.setBackground(Color.WHITE);
		panelGerente.setBounds(490, 10-100, 100, 25);
		panelGerente.add(labelGerente);
		// LOADING
		JLabel labelLoading = new JLabel();
		labelLoading.setBounds(100, 0, 600, 600);
		ImageIcon loadingGif = new ImageIcon("./media/Loading.png");
		loadingGif.getImage().flush();
		labelLoading.setIcon(loadingGif);
		panelLoading = new JPanel();
		panelLoading.setLayout(null);
		panelLoading.setBackground(Color.decode("#1DB0ED"));
		panelLoading.setBounds(0, -1000, 600, 600);
		panelLoading.add(labelLoading);
		
		// PORTA
		JLabel labelPonto = null;
		labelPonto = new JLabel("Ponto de acesso", JLabel.CENTER);
		labelPonto.setFont(new Font("Verdana", Font.PLAIN, 26));
		labelPonto.setForeground(Color.WHITE);
		labelPonto.setBackground(Color.decode("#1DB0ED"));
		labelPonto.setSize(600, 50);
		labelPonto.setOpaque(true);
		panelPonto = new JPanel();
		panelPonto.setLayout(null);
		panelPonto.setBounds(0, 100+300, 600, 50);
		panelPonto.add(labelPonto);
		
		
		
		
		// LAYEREDS TOP
		lpane = new JLayeredPane();
		lpane.setBounds(0, 0, 600, 100);
		lpane.add(panelIp, new Integer(1), 0);
		lpane.add(panelPorta, new Integer(2), 0);
		lpane.add(panelGerente, new Integer(3), 0);
		//lpane.add(panelLoading, new Integer(4), 0);
		lpane.add(panelTopCloud, new Integer(0), 0);

		// BOTAO ENVIAR ARQUIVO
		JButton buttonEnviarArquivo = new JButton("Enviar arquivo");
		buttonEnviarArquivo.setSize(285, 40);
		buttonEnviarArquivo.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panelButtonEnviarArquivo = new JPanel();
		panelButtonEnviarArquivo.setBackground(Color.WHITE);
		panelButtonEnviarArquivo.setLayout(null);
		panelButtonEnviarArquivo.setBounds(10 - 400, 100, 285, 40);
		panelButtonEnviarArquivo.add(buttonEnviarArquivo);
		buttonEnviarArquivo.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				enviarArquivo();
			}
		});
		// BOTAO CRIAR PASTA
		JButton buttonCriarPasta = new JButton("Criar pasta");
		buttonCriarPasta.setSize(285, 40);
		buttonCriarPasta.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panelButtonCriarPasta = new JPanel();
		panelButtonCriarPasta.setBackground(Color.WHITE);
		panelButtonCriarPasta.setLayout(null);
		panelButtonCriarPasta.setBounds(305 + 400, 100, 285, 40);
		panelButtonCriarPasta.add(buttonCriarPasta);
		buttonCriarPasta.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				criarPasta();
			}
		});
		// BOTAO MOVER ARQUIVO
		JButton buttonMoveArquivo = new JButton("Mover arquivo");
		buttonMoveArquivo.setSize(285, 40);
		buttonMoveArquivo.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panelButtonMoveArquivo = new JPanel();
		panelButtonMoveArquivo.setBackground(Color.WHITE);
		panelButtonMoveArquivo.setLayout(null);
		panelButtonMoveArquivo.setBounds(10 - 400, 150, 285, 40);
		panelButtonMoveArquivo.add(buttonMoveArquivo);
		buttonMoveArquivo.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				String path = labelPathArquivo.getText();
				String nomeArquivo = labelNomeArquivo.getText();
				System.out.println("moverArquivo("+path+nomeArquivo+",path)");
			}
		});
		// BOTAO DELETAR (PASTA/ARQUIVO)
		JButton buttonDelete = new JButton("Duplicar arquivos");
		buttonDelete.setSize(285, 40);
		buttonDelete.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panelButtonDelete = new JPanel();
		panelButtonDelete.setBackground(Color.WHITE);
		panelButtonDelete.setLayout(null);
		panelButtonDelete.setBounds(305 + 400, 150, 285, 40);
		panelButtonDelete.add(buttonDelete);
		buttonDelete.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				duplicarArquivos();
			}
		});
		
		// BOTAO MOVER ARQUIVO
		JButton buttonListarDiretorio = new JButton("Listar diretorio");
		buttonListarDiretorio.setSize(285, 40);
		panelButtonListarDiretorio = new JPanel();
		panelButtonListarDiretorio.setBackground(Color.WHITE);
		panelButtonListarDiretorio.setLayout(null);
		panelButtonListarDiretorio.setBounds(10 - 400, 200, 285, 40);
		panelButtonListarDiretorio.add(buttonListarDiretorio);
		buttonListarDiretorio.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				listarDiretorio();
			}
		});
		/*
		 * // BOTAO DELETAR (PASTA/ARQUIVO) JButton buttonDelete = new JButton(
		 * "Deletar pasta"); buttonDelete.setSize(285,40); panelButtonDelete =
		 * new JPanel(); panelButtonDelete.setBackground(Color.WHITE);
		 * panelButtonDelete.setLayout(null);
		 * panelButtonDelete.setBounds(305+400, 150, 285, 40);
		 * panelButtonDelete.add(buttonDelete);
		 */

		// TEXTO NOME DE ARQUIVO PREVIEW
		panelNomeArquivo = new JPanel();
		labelNomeArquivo = new JLabel("", JLabel.CENTER);
		labelNomeArquivo.setFont(new Font("Verdana", Font.PLAIN, 18));
		labelNomeArquivo.setForeground(Color.BLACK);
		labelNomeArquivo.setBackground(Color.WHITE);
		labelNomeArquivo.setSize(300, 60);
		labelNomeArquivo.setOpaque(true);
		panelNomeArquivo = new JPanel();
		panelNomeArquivo.setLayout(null);
		panelNomeArquivo.setBounds(300, 450 + 300, 300, 60);
		panelNomeArquivo.add(labelNomeArquivo);

		// TEXTO PATH DO ARQUIVO PREVIEW
		panelPathArquivo = new JPanel();
		labelPathArquivo = new JLabel("Carregando..", JLabel.CENTER);
		labelPathArquivo.setFont(new Font("Verdana", Font.PLAIN, 12));
		labelPathArquivo.setForeground(Color.GRAY);
		labelPathArquivo.setBackground(Color.WHITE);
		labelPathArquivo.setSize(300, 40);
		labelPathArquivo.setOpaque(true);
		panelPathArquivo = new JPanel();
		panelPathArquivo.setLayout(null);
		panelPathArquivo.setBounds(300, 510 + 300, 300, 40);
		panelPathArquivo.add(labelPathArquivo);
		
		// IMAGEM TIPO DADO PREVIEW
		JLabel labelUp = new JLabel();
		labelUp.setBounds(0, 0, 30, 30);
		labelUp.setIcon(new ImageIcon("media/Up.png"));
		labelUp.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panelUp = new JPanel();
		panelUp.setLayout(null);
		panelUp.setBackground(Color.WHITE);
		panelUp.setBounds(435, 210, 30, 30);
		panelUp.add(labelUp);
		labelUp.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				setAtualPath(Tool.subirPasta(getAtualPath()));
				listarDiretorio();
			}
		});

		// IMAGEM TIPO DADO PREVIEW
		labelIcon = new JLabel();
		labelIcon.setBounds(50, 0, 300, 200);
		labelIcon.setIcon(new ImageIcon("media/Desert.png"));
		labelIcon.setCursor(new Cursor(Cursor.HAND_CURSOR));
		panelIcon = new JPanel();
		panelIcon.setLayout(null);
		panelIcon.setBackground(Color.WHITE);
		panelIcon.setBounds(300 - 150, 250 - 50, 300, 200);
		panelIcon.add(labelIcon);
		labelIcon.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if(String.valueOf(labelIcon.getIcon()) == "media/Desert.png"){
					System.out.println("FAZ NADA");
				}else if(String.valueOf(labelIcon.getIcon()) == "media/File.png"){
					if(JOptionPane.showConfirmDialog(null, "Você clicou sobre o arquivo \""+labelNomeArquivo.getText()+"\"\nDeseja baixar ele para sua máquina?") == JOptionPane.OK_OPTION){
						baixarArquivo();
					}
				}else if(String.valueOf(labelIcon.getIcon()) == "media/Folder.png"){
					setAtualPath(labelPathArquivo.getText());
					listarDiretorio();
				}else{
					System.out.println("???");
				}
				
			}
		});

		// LISTA DE ITENS
		list = new JList(listagem);
		list.setSelectedIndex(0);
		list.setSize(1000, 1000);
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent evt) {
				refreshPreviewScreen(list.getSelectedIndex());
			}
		});
		refreshPreviewScreen(list.getSelectedIndex());

		// TELA DE SCROLL
		JScrollPane scroll = new JScrollPane(list);
		scroll.setBounds(0, 0, 290, 300);
		panelScroll = new JPanel();
		panelScroll.setLayout(null);
		panelScroll.setBackground(Color.WHITE);
		panelScroll.setBounds(10 - 500, 250, 290, 300);
		panelScroll.add(scroll);

		// JANELA
		// this = new JFrame("CloudIO");
		this.setTitle("CloudIO");
		this.setLayout(null);
		this.pack();
		this.getContentPane().setBackground(Color.decode("#1DB0ED"));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.add(lpane);
		this.add(panelPonto);
		//this.add(panelLoading);
		//this.add(panelButtonEnviarArquivo);
		//this.add(panelButtonCriarPasta);
		//this.add(panelButtonMoveArquivo);
		//this.add(panelButtonDelete);
		//this.add(panelButtonListarDiretorio);
		//this.add(panelScroll);
		//this.add(panelUp);
		//this.add(panelIcon);
		//this.add(panelNomeArquivo);
		//this.add(panelPathArquivo);
		this.setSize(600, 200);
		this.setLocationRelativeTo(null);
		this.setVisible(true);

		chegar();
		// pausa.aguardar();
	}

	// ATUALIZA A LISTAGEM DOS ARQUIVOS
	public void refreshList(ListaArquivos listaArquivos) {
		listaArquivos = listaArquivos.retirarDuplicatas();//
		System.out.println("LISTARDIRETORIo");
		setListagem(listaArquivos.getListagem());
		setTipo(listaArquivos.getTipo());
		list.setListData(getListagem());
		list.setSelectedIndex(0);
		refreshPreviewScreen(list.getSelectedIndex());
	}

	// ATUALIZA AS INFORMACOES DO ARQUIVO
	private void refreshPreviewScreen(int index) {
		String[] list = getListagem();
		int[] tipo = getTipo();
		if (list.length == 0) {
			labelIcon.setIcon(new ImageIcon("media/Desert.png"));
			labelNomeArquivo.setText("Pasta vazia!");
			labelPathArquivo.setText(getAtualPath());
		} else if (index >= 0){
			labelNomeArquivo.setText(list[index]);
			if (tipo[index] == FOLDER) {
				labelIcon.setIcon(new ImageIcon("media/Folder.png"));
				labelPathArquivo.setText(getAtualPath() + list[index] + "/");
			} else if (tipo[index] == FILE) {
				labelIcon.setIcon(new ImageIcon("media/File.png"));
				labelPathArquivo.setText(getAtualPath() + list[index]);
			}
		}
	}

	public String getAtualPath() {
		return atualPath;
	}

	public void setAtualPath(String atualPath) {
		this.atualPath = atualPath;
	}

	// EXECUTA OS EFEITOS DE TRANSICAO DE CHEGADA DOS COMPONENTES NA JANELA
	private void chegar() {
		transicaoTopCloud.mover(panelTopCloud, 0, 0, 0, 3000, Transicao.FAST_VERYSLOW);
		transicaoIp.mover(panelIp, 0, 10, 3500, 1000, Transicao.FAST_VERYSLOW);
		transicaoPorta.mover(panelPorta, 200, 10, 3800, 1000, Transicao.FAST_VERYSLOW);
		transicaoGerente.mover(panelGerente, 490, 10, 4100, 1000, Transicao.FAST_VERYSLOW);
		transicaoScroll.mover(panelScroll, 10, 250, 2000, 1000, Transicao.FAST_VERYSLOW);
		transicaoIcon.mover(panelIcon, 300, 250, 2000, 1000, Transicao.FAST_VERYSLOW);
		transicaoNomeArquivo.mover(panelNomeArquivo, 300, 450, 2500, 1000, Transicao.FAST_VERYSLOW);
		transicaoPathArquivo.mover(panelPathArquivo, 300, 510, 2700, 1000, Transicao.FAST_VERYSLOW);
		transicaoButton1.mover(panelButtonEnviarArquivo, 10, 100, 3000, 1000, Transicao.FAST_VERYSLOW);
		transicaoButton2.mover(panelButtonCriarPasta, 305, 100, 3000, 1000, Transicao.FAST_VERYSLOW);
		transicaoButton3.mover(panelButtonMoveArquivo, 10, 150, 3300, 1000, Transicao.FAST_VERYSLOW);
		transicaoButton4.mover(panelButtonDelete, 305, 150, 3300, 1000, Transicao.FAST_VERYSLOW);
		transicaoButton5.mover(panelButtonListarDiretorio, 10, 200, 3600, 1000, Transicao.FAST_VERYSLOW);
		transicaoPonto.mover(panelPonto,0,100,4000,3000, Transicao.FAST_VERYSLOW);
	}

	// EXECUTA OS EFEITOS DE TRANSICAO DE SAIDA E FINALIZA A JANELA
	private void sair() {
		transicaoTopCloud.mover(panelTopCloud, 0, 0, 0, 1000, Transicao.FAST_VERYSLOW);
	}

	// LISTA O DIRETORIO
	public void listarDiretorio() {
		path = "pasta/";
		comando = LISTAR_DIRETORIO;
		pausa.terminar();
	}

	//
	private void enviarArquivo() {
		JFileChooser fileChooser = new JFileChooser();
		int result = fileChooser.showOpenDialog(this);
		if (result == JFileChooser.APPROVE_OPTION) {
			File selectedFile = fileChooser.getSelectedFile();
			path = selectedFile.getAbsolutePath();
			comando = ENVIAR_ARQUIVO;
			pausa.terminar();
		}
	}
	//
	private void criarPasta(){
		String nomePasta = JOptionPane.showInputDialog("Digite o nome da nova pasta");
		setNomePasta(nomePasta);
		if(nomePasta != null){
			comando = CRIAR_PASTA;
			pausa.terminar();
		}
	}
	//
	private void duplicarArquivos(){
		comando = DUPLICAR_ARQUIVOS;
		pausa.terminar();
	}
	//
	
	private void baixarArquivo(){
		
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Escolha a pasta onde deseja salvar");
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int result = fileChooser.showOpenDialog(this);
		if (result == JFileChooser.APPROVE_OPTION) {
			File selectedFile = fileChooser.getSelectedFile();
			path = selectedFile.getAbsolutePath() + "/" + labelNomeArquivo.getText();
			setPathOrigem(labelPathArquivo.getText());
			comando = BAIXAR_ARQUIVO;
			pausa.terminar();
		}
	}
	//
	public int nextComando() {
		pausa.aguardar();
		return comando;
	}

	// public
	public String getPath() {
		return path;
	}

}
