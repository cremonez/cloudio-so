package view;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

import model.*;

public class Menu extends JFrame {
	public static final int ENTRAR = 0, ABRIR = 1, SAIR = -1;
	private int option;
	Transicao transicaoMenu = new Transicao();
	Transicao transicaoEntrarPartida = new Transicao();
	Transicao transicaoCriarPartida = new Transicao();
	Transicao transicaoSair = new Transicao();
	Pausa pausa = new Pausa();
	JLabel labelMenu, labelEntrarPartida, labelCriarPartida, labelSair;
	JPanel panelMenu, panelEntrarPartida, panelCriarPartida, panelSair;
	public Menu(){
		this.setTitle("CloudIO - Menu");
		this.setBackground(Color.WHITE);
		
		// IMAGEM NUVEM
		labelMenu = new JLabel();
		labelMenu.setBounds(0,0,600,600);
		labelMenu.setIcon(new ImageIcon("./media/Menu.png"));
		panelMenu = new JPanel();
		panelMenu.setLayout(null);
		panelMenu.setBackground(Color.WHITE);
		panelMenu.setBounds(0, 30 + 500, 600, 600);
		panelMenu.add(labelMenu);
		
		
		// TEXTO ENTRAR PARTIDA
		labelEntrarPartida = new JLabel("ENTRAR EM UMA NUVEM", JLabel.CENTER);
		labelEntrarPartida.setFont(new Font("Verdana",Font.PLAIN,20));
		labelEntrarPartida.setForeground(Color.WHITE);
		labelEntrarPartida.setBackground(Color.decode("#1DB0ED"));
		labelEntrarPartida.setSize(600, 60);
		labelEntrarPartida.setCursor(new Cursor(Cursor.HAND_CURSOR));
		labelEntrarPartida.setOpaque(true);
		panelEntrarPartida = new JPanel();
		panelEntrarPartida.setLayout(null);
		panelEntrarPartida.setBounds(0, 200 + 500, 600, 60);
		panelEntrarPartida.add(labelEntrarPartida);	
		labelEntrarPartida.addMouseListener(new MouseAdapter(){public void mouseEntered(MouseEvent e){selecionar(labelEntrarPartida);}});
		labelEntrarPartida.addMouseListener(new MouseAdapter(){public void mouseExited(MouseEvent e){deselecionar(labelEntrarPartida);}});
		labelEntrarPartida.addMouseListener(new MouseAdapter(){public void mouseClicked(MouseEvent e){setOption(ENTRAR); sair();}});
		
		// TEXTO CRIAR PARTIDA
		labelCriarPartida = new JLabel("CRIAR UMA NUVEM", JLabel.CENTER);
		labelCriarPartida.setFont(new Font("Verdana",Font.PLAIN,20));
		labelCriarPartida.setForeground(Color.WHITE);
		labelCriarPartida.setBackground(Color.decode("#1DB0ED"));
		labelCriarPartida.setSize(600, 60);
		labelCriarPartida.setCursor(new Cursor(Cursor.HAND_CURSOR));
		labelCriarPartida.setOpaque(true);
		panelCriarPartida = new JPanel();
		panelCriarPartida.setLayout(null);
		panelCriarPartida.setBounds(0, 270 + 500, 600, 60);
		panelCriarPartida.add(labelCriarPartida);		
		labelCriarPartida.addMouseListener(new MouseAdapter(){public void mouseEntered(MouseEvent e){selecionar(labelCriarPartida);}});
		labelCriarPartida.addMouseListener(new MouseAdapter(){public void mouseExited(MouseEvent e){deselecionar(labelCriarPartida);}});
		labelCriarPartida.addMouseListener(new MouseAdapter(){public void mouseClicked(MouseEvent e){setOption(ABRIR); sair();}});
		
		// TEXTO SAIR
		labelSair = new JLabel("SAIR", JLabel.CENTER);
		labelSair.setFont(new Font("Verdana",Font.PLAIN,20));
		labelSair.setForeground(Color.WHITE);
		labelSair.setBackground(Color.decode("#1DB0ED"));
		labelSair.setSize(600, 60);
		labelSair.setCursor(new Cursor(Cursor.HAND_CURSOR));
		labelSair.setOpaque(true);
		panelSair = new JPanel();
		panelSair.setLayout(null);
		panelSair.setBounds(0, 340 + 500, 600, 60);
		panelSair.add(labelSair);	
		labelSair.addMouseListener(new MouseAdapter(){public void mouseEntered(MouseEvent e){selecionar(labelSair);}});
		labelSair.addMouseListener(new MouseAdapter(){public void mouseExited(MouseEvent e){deselecionar(labelSair);}});
		labelSair.addMouseListener(new MouseAdapter(){public void mouseClicked(MouseEvent e){setOption(SAIR); sair();}});
		
		JLayeredPane lpane = new JLayeredPane();
		lpane.setBounds(0, 0, 600, 600);
        
        lpane.add(panelMenu, new Integer(0), 0);
      	lpane.add(panelCriarPartida, new Integer(1), 0);
      	lpane.add(panelEntrarPartida, new Integer(2), 0);
      	lpane.add(panelSair, new Integer(3), 0);
        
        
		// JANELA
		this.setLayout(null);
		this.pack();
		this.getContentPane().setBackground(Color.WHITE);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.add(lpane);
		this.setSize(600, 600);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		
		
		chegar();
		pausa.aguardar();
	}
	// EXECUTA OS EFEITOS DE TRANSICAO DE CHEGADA DOS COMPONENTES NA JANELA
	private void chegar(){
		transicaoMenu.mover(panelMenu, 0, 0, 0, 1000, Transicao.FAST_VERYSLOW);
		transicaoEntrarPartida.mover(panelEntrarPartida, 0, 200, 1500, 1000, Transicao.FAST_VERYSLOW);
		transicaoCriarPartida.mover(panelCriarPartida, 0, 270, 1800, 1000, Transicao.FAST_VERYSLOW);
		transicaoSair.mover(panelSair, 0, 340, 2100, 1000, Transicao.FAST_VERYSLOW);
	}
	// EXECUTA OS EFEITOS DE TRANSICAO DE SAIDA E FINALIZA A JANELA
	private void sair(){
		transicaoMenu.mover(panelMenu, 0, 1000, 1000, 1000, Transicao.VERYSLOW_FAST);
		transicaoEntrarPartida.mover(panelEntrarPartida, 0, 200+1000, 700, 1000, Transicao.VERYSLOW_FAST);
		transicaoCriarPartida.mover(panelCriarPartida, 0, 270+1000, 500, 1000, Transicao.VERYSLOW_FAST);
		transicaoSair.mover(panelSair, 0, 340+1000, 300, 1000, Transicao.VERYSLOW_FAST);
		new java.util.Timer().schedule(new java.util.TimerTask(){public void run(){fecharJanela();}},2500);
	}
	// FECHA A JANELA E VOLTA A EXECUTAR O CODIGO DO LOCAL ONDE FOI CHAMADA A FUNCAO
	private void fecharJanela(){
		this.dispose();
		pausa.terminar();
	}
	// MARCA COMO SELECIONADA A OPCAO LABEL
	private void selecionar(JLabel label){
		label.setForeground(Color.decode("#1DB0ED"));
		label.setBackground(Color.decode("#FFFFFF"));
	}
	// DESMARCA O SELECIONADO DA OPCAO LABEL
	private void deselecionar(JLabel label){
		label.setForeground(Color.decode("#FFFFFF"));
		label.setBackground(Color.decode("#1DB0ED"));
	}
	// SETA A OPCAO ESCOLHIDA
	public void setOption(int option){
		this.option = option;
	}
	// RETORNA A OPCAO ESCOLHIDA
	public int getOption(){
		return option;
	}

	
	
}
